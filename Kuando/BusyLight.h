//
//  BusyLight.h
//  LightControlMenu
//
//  Created by Eric Betts on 11/21/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>
#define SOUND_BASE 0x80
#define MAX_VOLUME 8
#define COMMAND_SIZE 14
enum positions {red = 2, green = 3, blue = 4, sound = 7};

/*
 OpenOffice        : 136,
 Quiet             : 144,
 Funky             : 152,
 FairyTale         : 160,
 KuandoTrain       : 168,
 TelephoneNordic   : 176,
 TelephoneOriginal : 184,
 TelephonePickMeUp : 192,
 Buzz              : 216
 }
 */


@interface BusyLight : NSObject {

}

@property IOHIDDeviceRef busylight;
-(void) initUsb;

+(BusyLight*) getInstance;
@property NSString *currentColor;
@property NSInteger timer_repeat;
@property NSInteger soundId;
@property NSInteger volume;

@end
