//
//  main.m
//  Kuando
//
//  Created by Eric Betts on 11/23/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>
#import "BusyLight.h"


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSString *color = @"white";
        int sound = 0;
        int volume = 0;
        if (argc > 1) {
            color = [NSString stringWithUTF8String:argv[1]];
        }
        if (argc > 2) {
          sound = atoi(argv[2]);
        }
        if (argc > 3) {
          volume = atoi(argv[3]);
        }

        //Spin off busylight daemon into its own thread
        BusyLight *busylight = [BusyLight getInstance];
        NSThread* busyLightDaemon = [[NSThread alloc] initWithTarget:busylight selector:@selector(initUsb) object:nil];
        [busyLightDaemon start];

        
        [[NSNotificationCenter defaultCenter] addObserverForName:@"deviceConnected" object:nil queue:nil usingBlock:^(NSNotification *notification)
         {
             //NSLog(@"%s: %@", __PRETTY_FUNCTION__, notification.userInfo);
             NSDictionary *userInfo = @{
                                        @"newColor": color,
                                        @"soundId": [NSNumber numberWithInteger:sound],
                                        @"volume": [NSNumber numberWithInteger:volume]
                                        };
             
             [[NSNotificationCenter defaultCenter] postNotificationName: @"setColor" object:nil userInfo:userInfo];
         }];
        
        [[NSRunLoop currentRunLoop] run];

    }
    return 0;
}
