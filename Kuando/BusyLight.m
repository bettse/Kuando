//
//  BusyLight.m
//  LightControlMenu
//
//  Created by Eric Betts on 11/21/13.
//  Copyright (c) 2013 Eric Betts. All rights reserved.
//

#import "BusyLight.h"

@implementation BusyLight

static BusyLight* singleton;

+(BusyLight*) getInstance
{
    if (!singleton){
        singleton = [[self alloc] init];
        singleton.currentColor = @"off";
        singleton.timer_repeat = 9;
        singleton.soundId = 0;
        singleton.volume = 0;
    }
    return singleton;
}

void colorWithHexColorString(NSString* inColorString, uint8_t* bytes) {
    unsigned colorCode = 0;
    unsigned char redByte, greenByte, blueByte;
    if (nil != inColorString) {
        NSScanner* scanner = [NSScanner scannerWithString:inColorString];
        (void) [scanner scanHexInt:&colorCode]; // ignore error
    }
    redByte = (unsigned char)(colorCode >> 16);
    greenByte = (unsigned char)(colorCode >> 8);
    blueByte = (unsigned char)(colorCode); // masks off high bits
    bytes[red] = redByte;
    bytes[green] = greenByte;
    bytes[blue] = blueByte;
}

- (NSData*) commandForColor:(NSString*)color
{
    uint8_t bytes[COMMAND_SIZE] = {0};
    if ([color isEqualToString:@"red"]) {
        bytes[red] = 0xFF;
    } else if ([color isEqualToString:@"green"]) {
        bytes[green] = 0xFF;
    } else if ([color isEqualToString:@"blue"]) {
        bytes[blue] = 0xFF;
    } else if ([color isEqualToString:@"white"]) {
        bytes[red] = 0xFF;
        bytes[green] = 0xFF;
        bytes[blue] = 0xFF;
    } else if ([color isEqualToString:@"off"] || [color isEqualToString:@""] || color == nil) {
        bytes[red] = 0;
        bytes[green] = 0;
        bytes[blue] = 0;
    } else {
        colorWithHexColorString(color, bytes);
    }

    return [NSData dataWithBytes:bytes length:sizeof(bytes)];
}

- (void) acceptNotification:(NSNotification *) notification
{
    NSDictionary *userInfo = notification.userInfo;
    //NSLog(@"%s: %@", __PRETTY_FUNCTION__, userInfo);
    self.currentColor = [userInfo[@"newColor"] lowercaseString];
    self.soundId = [userInfo[@"sound"] integerValue];
    self.volume = [userInfo[@"volume"] integerValue];
    [self updateColor];
}

- (void) updateColor
{
    NSData *command = [self commandForColor:self.currentColor];

    //Sound
    NSUInteger len = [command length];
    Byte *bytes = (Byte*)malloc(len);
    memcpy(bytes, [command bytes], len);

    bytes[sound] = SOUND_BASE + (self.soundId * 0x08) + self.volume;
    command = [NSData dataWithBytes:bytes length:len];

    long reportSize = 0;
    if (self.busylight) {
        IOHIDDeviceSetReport(self.busylight, kIOHIDReportTypeOutput, reportSize, (uint8_t *)[command bytes], [command length]);
    }
}

void BusyLightCallback(void *inContext, IOReturn result, void *sender, IOHIDReportType type, uint32_t reportID, uint8_t *report, CFIndex reportLength)
{
    if(reportLength == 0 || report[1] == 0) {
        return;
    }else if(reportLength == 64){
        NSData *data = [NSData dataWithBytes:report length:reportLength];
        NSLog(@"%s: %@", __FUNCTION__, [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding]);
    }
}

//http://developer.apple.com/library/mac/#documentation/DeviceDrivers/Conceptual/HID/new_api_10_5/tn2187.html
// function to get a long device property
// returns FALSE if the property isn't found or can't be converted to a long
static Boolean IOHIDDevice_GetLongProperty(IOHIDDeviceRef inDeviceRef, CFStringRef inKey, long * outValue)
{
    Boolean result = FALSE;
    CFTypeRef tCFTypeRef = IOHIDDeviceGetProperty(inDeviceRef, inKey);
    if (tCFTypeRef) {
        // if this is a number
        if (CFNumberGetTypeID() == CFGetTypeID(tCFTypeRef)) {
            // get its value
            result = CFNumberGetValue((CFNumberRef) tCFTypeRef, kCFNumberSInt32Type, outValue);
        }
    }
    return result;
}

// this will be called when the HID Manager matches a new (hot plugged) HID device
static void Handle_DeviceMatchingCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    @autoreleasepool {
        long reportSize = 0;
        BusyLight* self = (__bridge BusyLight*) inContext;
        self.busylight = inIOHIDDeviceRef;
        uint8_t *report;
        (void)IOHIDDevice_GetLongProperty(inIOHIDDeviceRef, CFSTR(kIOHIDMaxInputReportSizeKey), &reportSize);
        if (reportSize) {
            report = calloc(1, reportSize);
            if (report) {
                IOHIDDeviceRegisterInputReportCallback(inIOHIDDeviceRef, report, reportSize, BusyLightCallback, inContext);
                NSDictionary *userInfo = @{@"class": NSStringFromClass([self class])};
                //NSLog(@"%s %@", __PRETTY_FUNCTION__, userInfo);
                [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceConnected" object:nil userInfo:userInfo];

            }
        }
    }
}

// this will be called when a HID device is removed (unplugged)
static void Handle_DeviceRemovalCallback(void* inContext, IOReturn inResult, void* inSender, IOHIDDeviceRef inIOHIDDeviceRef)
{
    @autoreleasepool {
        BusyLight* self = (__bridge BusyLight*) inContext;
        self.busylight = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName: @"deviceDisconnected" object:nil userInfo:@{@"class": NSStringFromClass([self class])}];
    }
}

-(void) initUsb
{
    @autoreleasepool {
        const long vendorId = 0x04D8;
        const long productId = 0xF848;
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        IOHIDManagerRef managerRef = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDOptionsTypeNone);
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
        IOHIDManagerOpen(managerRef, 0L);
        dict[@kIOHIDProductIDKey] = @(productId);
        dict[@kIOHIDVendorIDKey] = @(vendorId);
        IOHIDManagerSetDeviceMatching(managerRef, (__bridge CFMutableDictionaryRef)dict);
        IOHIDManagerRegisterDeviceMatchingCallback(managerRef, Handle_DeviceMatchingCallback, (__bridge void *)(self));
        IOHIDManagerRegisterDeviceRemovalCallback(managerRef, Handle_DeviceRemovalCallback, (__bridge void *)(self));
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptNotification:) name:@"setColor" object:nil];
        //Setup timer to reassert color since Kuando Busylight automatically shuts off
        [NSTimer scheduledTimerWithTimeInterval:self.timer_repeat target:self selector:@selector(updateColor) userInfo:nil repeats:YES];
        //NSLog(@"Starting runloop");
        [[NSRunLoop currentRunLoop] run];
    }
}

@end
